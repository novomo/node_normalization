module.exports.findByValueOfObject = (key, value, obj) => {
  return obj.filter(function (item) {
    return item[key] === value;
  });
};
