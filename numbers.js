module.exports.toTimestamp = (d) => {
  let dt;
  if (typeof d === "string") {
    dt = new Date(d);
  } else {
    dt = d;
  }
  //console.log(dt);
  return parseInt(dt / 1000);
};
